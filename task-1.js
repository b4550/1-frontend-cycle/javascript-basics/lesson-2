// Convert the switch statement into an object called lookup.
// Use it to look up val and assign the associated string to the result variable.

// ! You should not use case, switch, or if statements

// Setup
function phoneticLookup(val) {
  let result = '';

  // Only change code below this line
  switch (val) {
    case 'alpha':
      result = 'Adams';
      break;
    case 'bravo':
      result = 'Boston';
      break;
    case 'charlie':
      result = 'Chicago';
      break;
    case 'delta':
      result = 'Denver';
      break;
    case 'echo':
      result = 'Easy';
      break;
    case 'foxtrot':
      result = 'Frank';
  }

  // Only change code above this line
  return result;
}

phoneticLookup('charlie'); // Chicago
phoneticLookup('alpha'); // Adams
phoneticLookup('bravo'); // Boston
phoneticLookup(''); // undefined
